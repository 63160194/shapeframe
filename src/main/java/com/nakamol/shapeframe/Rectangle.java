/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shapeframe;

/**
 *
 * @author OS
 */
public class Rectangle extends Shape {

    private double wide;
    private double height;

    public Rectangle(double wide, double height) {
        super("Rectangle");
        this.wide = wide;
        this.height = height;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public double calArea() {
        return wide * height;
    }

    @Override
    public double calPerimeter() {
        return 2 * (wide + height);
    }

}
