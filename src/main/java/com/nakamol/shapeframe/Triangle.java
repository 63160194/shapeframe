/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.shapeframe;

/**
 *
 * @author OS
 */
public class Triangle extends Shape {

    private double height;
    private double base;

    public Triangle(double height, double base) {
        super("Triangle");
        this.height = height;
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }
    

    @Override
    public double calArea() {
        return (height * base) / 2;
    }

    @Override
    public double calPerimeter() {
        double c = Math.sqrt((Math.pow(height, 2) + Math.pow(base, 2)));
        return c + height + base;
    }
}

